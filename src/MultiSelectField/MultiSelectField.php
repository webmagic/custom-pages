<?php

namespace Webmagic\CustomPages\MultiSelectField;

use Laracasts\Presenter\PresentableTrait;
use Webmagic\CustomPages\Field\Field;

class MultiSelectField extends Field
{
    use PresentableTrait;

    protected $presenter = MultiSelectFieldPresenter::class;


}