<?php


namespace Webmagic\CustomPages\MultiSelectField;

use Webmagic\Blog\Categories\CategoryRepo;
use Webmagic\Blog\Posts\PostRepo;
use Webmagic\CustomPages\Field\FieldPresenter;

class MultiSelectFieldPresenter extends FieldPresenter
{
    /**
     * Prepare and get content
     */
    public function getContent(){
        return $this->entity->content;
    }

    /**
     * Getting rendered view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function getView()
    {
        $view_path = 'custom_pages::dashboard.components.multiselect_field';
        $products = [];

        if (view()->exists($view_path)) {
            return view($view_path, [
                'field'=> $this->entity,
                'products'=> $products,
            ])->render();
        }

    }
}