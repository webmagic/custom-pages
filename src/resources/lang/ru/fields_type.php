<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Common translation for custom pages
    |--------------------------------------------------------------------------
    |
    */
    'text' => 'Текстовое поле',
    'textarea' => 'Текстовое поле (большое)',
    'textarea_editor' => 'Визуальный редактор',
    'image' => 'Изображение',
    'gallery' => 'Галлерея',
    'file' => 'Файл',
    'multiselect' => 'Мульти селект',
    'date_time' => 'Дата и время',
];