<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Common translation for custom pages
    |--------------------------------------------------------------------------
    |
    */
    'text' => 'Text field',
    'textarea' => 'Text area',
    'textarea_editor' => 'Visual editor',
    'image' => 'Image',
    'gallery' => 'Gallery',
    'file' => 'File',
    'multiselect' => 'Multi select',
    'date_time' => 'Date and time',
];