<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Common translation for custom pages
    |--------------------------------------------------------------------------
    |
    */
    'all-pages' => 'All pages',
    'add' => 'Add',
    'id' => 'ID',
    'title' => 'Title',
    'slug' => 'Slug',
    'delete' => 'Delete',
    'page-edition' => ':page page edition',
    'page-creation' => 'Page creation',
    'page-configuring' => ':page page configuring',
    'configure' => 'Configure',
    'pages' => 'Pages',
    'other-pages' => 'Other pages',
    'back' => 'Back to :page edition',
    'fields' => 'Fields',
    'key' => 'Key',
    'name' => 'Name',
    'type' => 'Type',
    'nothing-added' => 'Nothing added',
    'fields-groups' => 'Fields groups',
    'back-default' => 'Back',
    'field-edition' => 'Field edition',
    'field-creation' => 'Field creation',
    'required' => 'Required',
    'latin-charts' => 'Latin characters only',
    'save' => 'Save',
    'group-edition' => 'Fields group edition',
    'group-creation' => 'Fields group creation',
    'group-key-input' => 'Key (Try not to change)',
    'group' => 'Fields-group',
    'page-key-input' => 'Key (Try not to change)',
    'meta-title' => 'Meta title',
    'meta-description' => 'Meta description',
    'meta-keywords' => 'Meta keywords',
    'group-delete-all' => 'Delete all',
    'add-one' => 'Add item',
    'edit' => 'Configure'
];