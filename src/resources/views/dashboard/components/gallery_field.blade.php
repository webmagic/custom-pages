<div class="form-group">
    <div class="box">
        <div class="box-header with-border">
            <div class="box-title">{{$field['name']}}</div>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <label for="gallery_{{$field['id']}}" class="btn btn-box-tool bg-green" data-toggle="tooltip" title="Изменить"><i class="fa fa-edit"></i></label>
            </div>
        </div>
        <div class="box-body js_media-preview-gallery_{{$field['id']}}  img-gallery ">
                @if(count($field->present()->getContent) > 0)
                    @foreach($field->present()->getContent as $key => $img)
                        <div class="box col-md-12">
                            <div class="box-body col-md-3">
                                <img style="width: 300px;" class="img-responsive" src="{{$img}}" alt="">
                            </div>
                            <div class="box-footer col-md-9">
                                <a target="_blank" href="{{$img}}">{{$img}}</a>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="img-blk">
                        <img class="img-responsive" src="{{url('webmagic/dashboard/img/img-placeholder.png')}}" alt="">
                    </div>
                @endif
        </div>
        <input name="file_{{$field['id']}}_update" type="hidden" data-original-title="" title="">
        <label for="image_{{$field['id']}}" class="hidden" data-original-title="" title="">Основное изображение</label>
        <input data-file-name="file_{{$field['id']}}" multiple class=" js_image-preview hidden" accept="image/*" data-preview=".js_media-preview-gallery_{{$field['id']}}" name="field[][{{$field['id']}}]" type="file" id="gallery_{{$field['id']}}">
    </div>
</div>