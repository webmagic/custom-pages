
<label for="short_description" data-original-title="" title="">{{$field['name']}}</label>

<div class="form-group">
    <textarea class="js-editor  form-control" cols="50" rows="10" id="short_description"
              name="field[][{{$field['id']}}]">
        {{$field['content']}}
    </textarea>
</div>

