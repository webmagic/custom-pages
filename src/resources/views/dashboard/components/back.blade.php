<nav>
    <ul class="pager">
        <li class="previous">
            <a @if(isset($link_for_back)) href="{{$link_for_back}}" @else href="{{ URL::previous() }}" @endif>
                <span aria-hidden="true">&larr;</span>
                @if(isset($text_back_btn))
                    {!! $text_back_btn !!}
                @else
                    @lang('custom-pages::common.back-default')
                @endif
            </a>
        </li>
    </ul>
</nav>