<div class="form-group" data-original-title="" title="">
    <label for="multiselect_{{$field['id']}}">{{$field['name']}}</label>
    <select id="multiselect_{{$field['id']}}" name="field[][{{$field['id']}}]" class="js-select2 col-md-12 " multiple="" tabindex="-1" >
        @if(isset($products) && count($products))
            @foreach($products as $key=>$product)
                <option value="{{$product}}">{{$product}}</option>
            @endforeach
        @endif
    </select>
</div>