<div class="form-group">
    <div class="box">
        <div class="box-header with-border">
            <div class="box-title">{{$field['name']}}</div>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <label for="image_{{$field['id']}}" class="btn btn-box-tool bg-green" data-toggle="tooltip" title="Изменить"><i class="fa fa-edit"></i></label>
            </div>
        </div>

        <div class="box-body js_media-preview-image_{{$field['id']}}">

            @if($field['content'])
                <img class="img-responsive" src="{{$field->present()->getContent}}" alt="">
            @else
                <div class="img-blk">
                    <img class="img-responsive" style="max-width: 300px;" src="{{url('webmagic/dashboard/img/img-placeholder.png')}}" alt="">
                </div>
            @endif

        </div>
        <input name="file_{{$field['id']}}_update"  type="hidden">
        <label for="image_{{$field['id']}}" class="hidden">Основное изображение</label>
        <input data-file-name="file_{{$field['id']}}" class=" js_image-preview hidden" accept="image/*" data-preview=".js_media-preview-image_{{$field['id']}}" name="field[][{{$field['id']}}]" type="file" id="image_{{$field['id']}}" data-original-title="" title="">
    </div>
</div>