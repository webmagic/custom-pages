<label for="date-{{$field['key']}}">{{$field['name']}}</label>
<div class="form-group">
    <input class="form-control js_date_range_picker" value="{{$field->present()->getContent}}" id="date-{{$field['key']}}" name="field[][{{$field['id']}}]">
</div>