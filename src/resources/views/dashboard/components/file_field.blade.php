<div class="form-group">
    <label for="file-{{$field['id']}}" >{{$field['name']}}</label>

    @if($field['content'])
        <div class="mailbox-attachments clearfix">
        <div style="width: 200px;">
            <span class="mailbox-attachment-icon"><i class="fa  fa-file"></i></span>

            <div class="mailbox-attachment-info">
                <a target="_blank" href="{{$field->present()->getContent}}" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i>{{$field['content']}}</a>
            </div>
        </div>
        </div>
    @endif
    <input data-file-name="file_{{$field['id']}}" name="field[][{{$field['id']}}]" type="file" id="file-{{$field['id']}}">
    <input name="file_{{$field['id']}}_update" type="hidden">
</div>