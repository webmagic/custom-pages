<h1>@lang('custom-pages::common.page-creation')</h1>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                {!! $form_builder->model($page, ['url' => '/dashboard/custom-pages/page', 'class' => 'js-submit', 'method' => 'POST']) !!}
                @include('custom_pages::dashboard._form')
                {!! $form_builder->close() !!}
            </div>
        </div>
    </div>
</div>