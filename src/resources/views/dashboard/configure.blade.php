@include('custom_pages::dashboard.components.back', [
    'link_for_back'=>route('custom_pages_module::pages::edit', $page->key),
    'text_back_btn' => __('custom-pages::common.back', ['page' => $page->title])
])
<h1>@lang('custom-pages::common.page-configuring', ['page' => $page->title])</h1>

{{--Fields--}}
@include('custom_pages::dashboard.field._list', [
'createLink' => route('custom_pages_module::fields::create', [$item_type, $page['key']])
])
{{--Fields--}}

<div class="box">
    <div class="box-header">
        <h1>@lang('custom-pages::common.fields-groups')
            <a href="{{route('custom_pages_module::fields_group::create', $page['key'])}}"
               class="btn btn-primary pull-right"><i class="fa fa-plus"></i> @lang('custom-pages::common.add')</a>
        </h1>

    </div>
    <div class="box-body">
        @if(count($fields_group))
            <table class="table table-condensed">
                <tr>
                    <th>@lang('custom-pages::common.id')</th>
                    <th>@lang('custom-pages::common.key')</th>
                    <th>@lang('custom-pages::common.name')</th>
                    <th></th>
                </tr>
                @foreach($fields_group as $key => $group)
                    <tr class="js_field_group_{{$group['id']}}">
                        <td style="width: 100px;">{{$group['id']}}</td>
                        <td>{{$group['key']}}</td>
                        <td>{{$group['name']}}</td>
                        <td width="200px">
                            <div class="btn-group">
                                <a href="{{route('custom_pages_module::fields_group::edit', $group['key'] . '_child')}}"
                                   class="btn btn-info"
                                   type="button"><i class="fa fa-cog"></i></a>
                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a class="js_delete category-delete"
                                           data-item=".js_field_group_{{$group['id']}}"
                                           data-method="DELETE"
                                           data-request="{{route('custom_pages_module::fields_group::destroy', $group['key'])}}">
                                            @lang('custom-pages::common.delete')</a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        @else
            <p class="text-center">@lang('custom-pages::common.nothing-added')</p>
        @endif
    </div>
</div>
