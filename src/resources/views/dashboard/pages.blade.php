<h1>@lang('custom-pages::common.all-pages')</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="pull-right">
                    <a href="{{url('dashboard/custom-pages/page/create')}}" class="btn btn-sm btn-info pull-right personals-btn products-btn"><i class="fa fa-plus"> </i> @lang('custom-pages::common.add')</a>
                </div>
                <div class="box-body">
                    <table class="table table-hover dataTable">
                        <thead>
                        <tr>
                            <th>@lang('custom-pages::common.id')</th>
                            <th>@lang('custom-pages::common.title')</th>
                            <th>@lang('custom-pages::common.slug')</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pages as $key => $page)
                            <tr role="row" class="even products-row js_field_{{$key}}">
                                <td class="sorting_1">{!! $page->id !!}</td>
                                <td>{{$page->title}}</td>
                                <td>{{$page->slug}}</td>
                                <td>
                                <td width="200px">
                                    <div class="btn-group">
                                        <a href="{{route('custom_pages_module::pages::edit', $page->key)}}"
                                           class="btn btn-info"><i class="fa fa-pencil-square-o"></i></a>

                                        <a href="{{route('custom_pages_module::pages::configure', $page->key)}}"
                                           class="personals-edit btn btn-info btn-flat"><i class="fa fa-cog"></i></a>
                                        <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                              <li>
                                                <a   data-method="DELETE"
                                                     data-request="{{route('custom_pages_module::pages::destroy', $page->key)}}"
                                                     class="js_delete personals-delete"
                                                     data-item=".js_field_{{$key}}">
                                                    @lang('custom-pages::common.delete')</a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


