@extends('dashboard::base')

@section('title', 'Редактирование')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    {!! $form_builder->model($widget, ['url' => '/dashboard/custom-pages/widget/'.$widget['id'], 'class' => 'js-submit', 'method' => 'PUT']) !!}
                    @include('custom_pages::dashboard.widgets._form')
                    {!! $form_builder->close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection