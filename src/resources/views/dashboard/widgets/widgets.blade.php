@extends('dashboard::base')

@section('title', 'Все виджеты')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="pull-right">
                    <a href="{{url('dashboard/custom-pages/widget/create')}}" class="btn btn-sm btn-info pull-right personals-btn products-btn" title="Добавить новый блок"><i class="fa  fa-plus"> </i> Новй виджет</a>
                </div>
                <div class="box-body">
                    <table class="table table-hover dataTable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <th>Slug</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($widgets as $widget)
                            <tr role="row" class="even products-row">
                                <td class="sorting_1">{!! $widget['id'] !!}</td>
                                <td>{{$widget['name']}}</td>
                                <td>{{$widget['slug']}}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" data-method="DELETE" data-request="/dashboard/custom-pages/widget/{!! $widget['id'] !!}"
                                                class="js_delete personals-delete btn btn-danger btn-flat"><i class="fa fa-trash"></i></button>
                                        <a href="{{url('/dashboard/custom-pages/widget/' .$widget['id'] . '/edit')}}" class="personals-edit btn btn-info btn-flat">
                                            <i class="fa fa-pencil-square-o"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection


