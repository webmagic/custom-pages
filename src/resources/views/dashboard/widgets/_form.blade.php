<div class="form-group">
    <label for="page_name" class="control-label">Имя</label>
    {!! $form_builder->text('name', null, ['class'=>'form-control']) !!}
</div>
<div class="form-group">
    <label for="page_price" class="control-label">Slug</label>
    {!! $form_builder->text('slug', null, ['class'=>'form-control']) !!}
</div>
<div class="form-group">
    <label>Content</label>
    {!! $form_builder->textarea('content', null, ['class'=>'form-control js-editor']) !!}
</div>

<div class="box-footer">
    <button type="submit" class="btn btn-primary">Сохранить</button>
</div>

