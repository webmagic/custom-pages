<h1>@lang('custom-pages::common.page-edition', ['page' => $page->title])</h1>
<div class="row">
    <div class="col-xs-12">
        {!! $form_builder->model($page, ['url' => '/dashboard/custom-pages/page/'.$page->id, 'class' => 'js-submit', 'method' => 'PUT']) !!}
        <div class="box">
            <div class="box-header">
                <a href="{{route('custom_pages_module::pages::configure', $page->key)}}"
                   class="btn btn-info pull-right"> <i class="fa fa-cog"></i> @lang('custom-pages::common.configure')
                </a>
            </div>
            <div class="box-body">
                @include('custom_pages::dashboard._form', ['edit' => true])
            </div>
        </div>

        @section('fields-groups-tabs-footer')
            {{--Field groups--}}
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary pull-right">@lang('custom-pages::common.save')</button>
            </div>
        @endsection

        {{--Field groups--}}
        @if(isset($field_groups) && count($field_groups))
            @include('custom_pages::dashboard.field_group._tabs')
        @endif

        {!! $form_builder->close() !!}
    </div>
</div>

