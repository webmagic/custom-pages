<div class="row"></div>
<div class="col-md-12"></div>

<div class="form-group">
    <label for="page_name" class="control-label">@lang('custom-pages::common.title')</label>
    {!! $form_builder->text('title', null, ['class'=>'form-control']) !!}
</div>
@if($routes_registration_status)
    <div class="form-group">
        <label for="page_price" class="control-label">@lang('custom-pages::common.slug'): </label>
        <div class="input-group">
            <span class="input-group-addon">{{url($page_url_prefix)}}/</span>
            {!! $form_builder->text('slug', null, ['class'=>'form-control']) !!}
        </div>
    </div>
@endif
{!! $component_generator->text('key', null, __('custom-pages::common.page-key-input')) !!}

{!! $component_generator->text('meta_title', null, __('custom-pages::common.meta-title')) !!}
{!! $component_generator->text('meta_description', null, __('custom-pages::common.meta-description')) !!}
{!! $component_generator->text('meta_keywords', null, __('custom-pages::common.meta-keywords')) !!}

{{--Fields--}}
@if(isset($fields) && count($fields))
    @foreach($fields as $key => $field)
        {!!  $field->present()->getView() !!}
    @endforeach
@endif
{{--Fields--}}

<div class="col-md-12">
    <button type="submit" class="btn btn-primary pull-right">@lang('custom-pages::common.save')</button>
</div>