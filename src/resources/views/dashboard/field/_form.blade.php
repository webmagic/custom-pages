{!! $component_generator->text('name', null, __('custom-pages::common.name').'*') !!}

<div class="form-group">
    <label for="field_type">@lang('custom-pages::common.type')*</label>
    <select id="field_type" name="type" class=" form-control ">
        @foreach($fields_type as $key => $type)
            <option value="{{$key}}" @if(isset($field['type']) && $field['type'] === $key) selected @endif>
                @lang("custom-pages::fields_type.$key")
            </option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="field_key">@lang('custom-pages::common.type') (@lang('custom-pages::common.required'), @lang('custom-pages::common.latin-charts'))*</label>
    @if(isset($field['field_key']))
        <input @if($item_type === 'group') disabled @endif id="field_key" required class=" form-control" type="text" value="{{$field['field_key']}}" name="field_key">
    @else
        <input id="field_key" required class=" form-control" type="text" value="" name="field_key">
    @endif
</div>

<input type="hidden" value="{{$item_key}}" name="item_key">
<input type="hidden" value="{{$item_type}}" name="item_type">

<div class="box-footer">
    <button type="submit" class="btn btn-primary">@lang('custom-pages::common.save')</button>
</div>

