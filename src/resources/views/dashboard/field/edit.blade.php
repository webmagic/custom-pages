@include('custom_pages::dashboard.components.back')

<h1>@lang('custom-pages::common.field-edition')</h1>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    {!! $form_builder->model($field, ['url' => route('custom_pages_module::fields::update', [$field['id']]), 'class' => 'js-submit', 'method' => 'PUT']) !!}
                    @include('custom_pages::dashboard.field._form')
                    {!! $form_builder->close() !!}
                </div>
            </div>
        </div>
    </div>
