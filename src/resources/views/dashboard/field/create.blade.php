@include('custom_pages::dashboard.components.back')

<h1>@lang('custom-pages::common.field-creation')</h1>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    {!! $form_builder->model($field, ['url' => '/dashboard/custom-pages/field', 'class' => 'js-submit', 'method' => 'POST']) !!}
                    @include('custom_pages::dashboard.field._form')
                    {!! $form_builder->close() !!}
                </div>
            </div>
        </div>
    </div>
