<div class="box">
    <div class="box-header">
        <h1>@lang('custom-pages::common.fields') <a href="{{$createLink}}"
                                                    class="btn btn-primary pull-right"><i class="fa fa-plus"></i> @lang('custom-pages::common.add')</a></h1>
    </div>
    <div class="box-body">
        @if(count($fields))
            <table class="table table-hover">
                <tr>
                    <th>@lang('custom-pages::common.id')</th>
                    <th>@lang('custom-pages::common.key')</th>
                    <th>@lang('custom-pages::common.name')</th>
                    <th>@lang('custom-pages::common.type')</th>
                    <th></th>
                </tr>
                @foreach($fields as $key => $field)
                    <tr class="js_field_{{$field['id']}}">
                        <td style="width: 100px;">{{$field['id']}}</td>
                        <td>{{$field['field_key']}}</td>
                        <td>{{$field['name']}}</td>
                        <td>{{$field['type']}}</td>
                        <td width="200px">
                            <div class="btn-group">
                                <a href="{{route('custom_pages_module::fields::edit', $field['id'])}}" class="btn btn-info"
                                   type="button"><i class="fa fa-cog"></i></a>
                                <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a   class="js_delete category-delete"
                                             data-item=".js_field_{{$field['id']}}"
                                             data-method="DELETE"
                                             data-request="{{route('custom_pages_module::fields::destroy', $field['id'])}}">
                                            @lang('custom-pages::common.delete')</a>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        @else
            <p class="text-center">@lang('custom-pages::common.nothing-added')</p>
        @endif
    </div>
</div>