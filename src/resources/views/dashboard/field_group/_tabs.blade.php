<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        @foreach($field_groups as $key => $group)
            <li @if($key === 0) class="active" @endif>
                <a href="#tab_{{$group['id']}}" data-toggle="tab" aria-expanded="true">{{$group['name']}}</a>
            </li>
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach($field_groups as $key_group => $group)
            <div class="tab-pane @if($key_group === 0) active @endif" id="tab_{{$group['id']}}">
                <div class="row">
                    <div class="col-md-12">
                        {{--field group start--}}
                        <div class="js_parent-group_{{$group['key']}}">
                            {{--header--}}
                            <div class="box-header">
                                <div class="box-title">
                                    <b>{{$group['name']}}</b>
                                </div>
                                <div class="box-tools pull-right">
                                    <a href="#"
                                       class="btn btn-sm btn-danger js_delete personals-delete"
                                       data-method="DELETE"
                                       data-request="{{route('custom_pages_module::fields_group::destroy-parent', [$group['key']])}}"
                                       data-item=".js_parent-group_{{$group['key']}}"
                                    >
                                        <i class="fa fa-trash"></i> @lang('custom-pages::common.group-delete-all')
                                    </a>
                                    <a href="{{route('custom_pages_module::fields_group::edit', $group['key'] . '_child')}}"
                                       class="btn btn-default btn-sm">
                                        <i class="fa fa-cog"></i> @lang('custom-pages::common.edit')
                                    </a>
                                    <a href="{{route('custom_pages_module::fields_group::copy', $group['key'])}}"
                                       class="btn btn-default btn-sm">
                                        <i class="fa fa-copy"></i> @lang('custom-pages::common.add-one')
                                    </a>
                                </div>

                            </div>
                            {{--header end--}}

                            {{--SECOND LEVEL--}}
                            <div class="box-body">
                                @if(isset($group['groups']) && count($group['groups']))
                                    @foreach($group['groups'] as $key => $group)
                                        {{--field group start--}}
                                        <div class="box box-primary js_group_{{$key}}">
                                            {{--header--}}
                                            <div class="box-header">
                                                <div class="pull-right">
                                                    @if($key != 0)
                                                        <a href="#" type="button"
                                                           data-method="DELETE"
                                                           data-request="{{route('custom_pages_module::fields_group::destroy', [$group['key']])}}"
                                                           class="js_delete  btn btn-danger"
                                                           data-item=".js_group_{{$key}}"
                                                        >
                                                            @lang('custom-pages::common.delete')
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                            {{--header end--}}

                                            {{--FIELDS RENDER--}}
                                            <div class="box-body">
                                                @if(isset($group['fields']) && count($group['fields']))
                                                    @foreach($group['fields'] as $key=>$field)
                                                        {!!  $field->present()->getView() !!}
                                                    @endforeach

                                                @endif
                                            </div>
                                            {{--FIELDS RENDER END--}}
                                        </div>
                                        {{--field group END--}}
                                    @endforeach

                                @endif
                            </div>
                            {{--SECOND LEVEL end--}}
                        </div>
                        {{--field group END--}}


                    </div> {{--END col--}}
                </div> {{--END row--}}
            </div> {{--END tab-pane--}}
        @endforeach
    </div> {{--END tab content--}}
    <div class="box-footer">
        @yield('fields-groups-tabs-footer')
    </div>
</div>


