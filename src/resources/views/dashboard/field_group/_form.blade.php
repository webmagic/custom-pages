<div class="form-group">
    <label for="page_name" class="control-label">@lang('custom-pages::common.name')</label>
    {!! $form_builder->text('name', null, ['class'=>'form-control']) !!}
</div>
{!! $component_generator->text('key', null, __('custom-pages::common.group-key-input')) !!}

<input type="hidden" name="page_key" value="{{$page_key}}">

<div class="col-md-12" style="margin-top: 20px;">
<button type="submit" class="btn btn-primary">@lang('custom-pages::common.save')</button>
</div>

