@include('custom_pages::dashboard.components.back')

<h1>@lang('custom-pages::common.group-creation')</h1>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    {!! $form_builder->model(null , ['url' => route('custom_pages_module::fields_group::store'), 'class' => 'js-submit', 'method' => 'POST']) !!}
                    @include('custom_pages::dashboard.field_group._form')
                    {!! $form_builder->close() !!}
                </div>
            </div>
        </div>
    </div>
