@include('custom_pages::dashboard.components.back',
['link_for_back'=>route('custom_pages_module::pages::edit', $field_group->page_key),
])

<h1>@lang('custom-pages::common.group-edition')</h1>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    {!! $form_builder->model($field_group, ['url' => route('custom_pages_module::fields_group::update', $field_group['id']), 'class' => 'js-submit', 'method' => 'PUT']) !!}
                    @include('custom_pages::dashboard.field_group._form', ['edit'=>true])
                    {!! $form_builder->close() !!}
                </div>
            </div>
        </div>
    </div>

{{--Fields--}}
@include('custom_pages::dashboard.field._list', [
    'createLink' => route('custom_pages_module::fields::create', [$item_type, $field_group['key']])
])
{{--Fields--}}