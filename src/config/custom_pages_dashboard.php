<?php
/*
   |--------------------------------------------------------------------------
   | Module Custom Pages. Dashboard settings
   |--------------------------------------------------------------------------
   |
   | This setting needs only if you want use module Dashboard
   |
   */
return [

    /*
    |--------------------------------------------------------------------------
    | Module routes_dashboard prefix
    |--------------------------------------------------------------------------
    |
    | This prefix use for generation all routes for module
    |
    */
    'prefix_for_custom_pages' => 'dashboard/custom-pages',

    'hash_use' => false,



    /*
    |--------------------------------------------------------------------------
    | Middleware
    |--------------------------------------------------------------------------
    |
    | Can use middleware for access to module page
    |
    */
    'middleware' => ['custom_pages', 'auth'],


    /*
    |--------------------------------------------------------------------------
    | Menu configuration
    |--------------------------------------------------------------------------
    |
    */
    'add_pages' => true,
    
    'dashboard_menu_item' => [
        'link' => 'dashboard/custom-pages',
        'text' => 'custom-pages::common.other-pages',
        'icon' => 'fa-list',
        'subitems' => [
             [
                'link' => 'dashboard/custom-pages/page',
                'text' => 'custom-pages::common.pages',
                'icon' => ' fa-files-o',
                 'active_rules' => [
                     'routes_parts'=>[
                         'custom_pages_module::pages::',
                         'custom_pages_module::fields::'
                     ],
                 ]
            ],
//            [
//                'link' => 'dashboard/custom-pages/widget',
//                'text' => 'Виджеты',
//                'icon' => ' fa-th-large',
//            ],

        ],

    ]

];


