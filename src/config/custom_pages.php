<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Use for on/off custom pages routes registering
    |--------------------------------------------------------------------------
    |
    | If needs add additional field add it in migration
    | and add there for activate saving data
    |
    */

    'pages_routes_register' => true,
    'widgets_routes_register' => true,

    /*
    |--------------------------------------------------------------------------
    | Module routes_dashboard prefix
    |--------------------------------------------------------------------------
    |
    | This prefix use for generation all routes for module
    |
    */

    'prefix_for_custom_pages' => 'page',
    'prefix_for_widget' => 'widget',

    /*
    |--------------------------------------------------------------------------
    | Fields available in model additional to base fields
    |--------------------------------------------------------------------------
    |
    | If needs add additional field add it in migration
    | and add there for activate saving data
    |
    */

    'pages_available_fields' => [],
    'widgets_available_fields' => [],

    'img_path' => 'webmagic/custom-pages/img/',
    'file_path' => 'webmagic/custom-pages/files/',

    'fields_type' => [
        'text' => [
            'name' => 'Текстовое поле',
            'class' => \Webmagic\CustomPages\Field\Field::class
        ],
        'textarea'=>[
            'name' => 'Текстовое поле (большое)',
            'class' => \Webmagic\CustomPages\Field\Field::class
        ],
        'textarea_editor'=>[
            'name' => 'Визуальный редактор',
            'class' => \Webmagic\CustomPages\Field\Field::class
        ],
        'image' => [
            'name' => 'Картинка',
            'class' => \Webmagic\CustomPages\Field\Field::class
        ],
        'gallery' => [
            'name' => 'Галерея',
            'class' => \Webmagic\CustomPages\Field\Field::class
        ],
        'file' => [
            'name' => 'Файл',
            'class' => \Webmagic\CustomPages\Field\Field::class
        ],
        'multiselect' => [
            'name' => 'Мульти Селект',
            'class' => \Webmagic\CustomPages\MultiSelectField\MultiSelectField::class
        ],
        'date_time' => [
            'name' => 'Дата и время',
            'class' => \Webmagic\CustomPages\Field\Field::class
        ],
    ]

];