<?php

namespace Webmagic\CustomPages;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Faker\Generator as FakerGenerator;
use Illuminate\Database\Eloquent\Factory as EloquentFactory;

class CustomPagesServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/config/custom_pages.php', 'webmagic.custom_pages'
        );

        $this->registerFactories();
    }

    /**
     * Boot pages service
     * @param Router $router
     */
    public function boot(Router $router)
    {
        //Migrations publishing
        $this->publishes([
            __DIR__.'/database/migrations/' => database_path('/migrations'),
        ], 'migrations');

        //Seeds publishing
        $this->publishes([
            __DIR__.'/database/seeds/' => database_path('/seeds'),
        ], 'seeds');

        //Config publishing
        $this->publishes([
            __DIR__ . '/config/custom_pages.php' => config_path('webmagic/custom_pages.php'),
        ], 'config');

        //Load Views
        $this->loadViewsFrom(__DIR__.'/resources/views', 'custom_pages');

        $this->registerMiddleware($router);
        $this->loadMigrations();
    }


    /**
     * Middleware registration
     *
     * @param $router
     */
    protected function registerMiddleware($router)
    {
        $router->middlewareGroup('custom_pages', [
            \Illuminate\Cookie\Middleware\EncryptCookies::class,
            \Illuminate\Session\Middleware\StartSession::class,
        ]);
    }


    /**
     * Factories registration
     */
    protected function registerFactories()
    {
        $this->app->singleton(EloquentFactory::class, function ($app){
            return EloquentFactory::construct($app->make(FakerGenerator::class),  __DIR__.'/database/factories/');
        });
    }

    /**
     * Load migrations
     */
    protected function loadMigrations()
    {
        $this->loadMigrationsFrom(__DIR__.'/database/migrations/');
    }

}
