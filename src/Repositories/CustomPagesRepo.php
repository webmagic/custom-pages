<?php

namespace Webmagic\CustomPages\Repositories;


use Illuminate\Database\Eloquent\Model;
use Webmagic\Core\Entity\EntityRepo;
use Webmagic\CustomPages\Entities\CustomPage;
use Illuminate\Database\Eloquent\Builder;

class CustomPagesRepo extends EntityRepo  implements CustomPagesRepoContract
{

    protected $entity = CustomPage::class;


    /**
     * Return page by Slug
     *
     * @param $slug
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function getBySlug(string $slug)
    {
        $query = $this->query();
        $query->where('slug', $slug)->with('fields');

        return $this->realGetOne($query);
    }

    /**
     * Get entity by param page_key
     *
     * @param $page_key
     * @return \Illuminate\Database\Eloquent\Model
     * @internal param int|string $entity_id
     *
     */
    public function getByKey($page_key)
    {
        $query = $this->query();
        $query->where('key', $page_key);

        return $this->realGetOne($query);
    }


    /**
     * Save field with assigning to CustomPage entity
     *
     * @param $field
     * @param $page_key
     */
    public function saveWithAssign($field, $page_key)
    {
        $page = $this->getByKey($page_key);
        $page->fields()->save($field);

    }

    /**
     * Get one entity based on query
     *
     * @param Builder $query
     *
     * @return Model|null
     */
    protected function realGetOne(Builder $query)
    {
        $query = $query->with('fields')->with('fieldGroup');

        return $query->first();
    }

    public function getIdOfRelatedFields(Model $page)
    {
        $fields_id = [];

        foreach ($page['fields'] as $field){
            $fields_id[] = $field['id'];
        }

        return $fields_id;
    }

    /**
     *  Get all entities based on query
     *
     * @param Builder $query
     *
     * @return Collection|Paginator
     */
    protected function realGetMany(Builder $query)
    {
        $query = $query->with('fields')->with('fieldGroup');

        return $query->get();
    }


}
