<?php

namespace Webmagic\CustomPages\Repositories;


use Webmagic\Core\Entity\EntityRepo;
use Webmagic\CustomPages\Entities\Widget;

class WidgetRepo extends EntityRepo implements WidgetRepoContract
{

    protected $entity = Widget::class;

    /**
     * Return widget by Slug
     *
     * @param $slug
     * @return \Illuminate\Database\Eloquent\Model|null
     *
     */
    public function getBySlug(string $slug)
    {
        $query = $this->query();
        $query->where('slug', $slug);

        return $this->realGetOne($query);
    }
}
