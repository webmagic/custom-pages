<?php

namespace Webmagic\CustomPages\Repositories;


use Webmagic\Core\Entity\EntityRepoInterface;

interface CustomPagesRepoContract extends EntityRepoInterface
{

    /**
     * Return page by Slug
     *
     * @param $slug
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function getBySlug(string $slug);
}
