<?php

namespace Webmagic\CustomPages\Repositories;


use Webmagic\Core\Entity\EntityRepoInterface;

interface WidgetRepoContract extends EntityRepoInterface
{

    /**
     * Return widget by Slug
     *
     * @param $slug
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function getBySlug(string $slug);
}
