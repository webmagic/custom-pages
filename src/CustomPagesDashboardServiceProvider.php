<?php

namespace Webmagic\CustomPages;


use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Schema;
use Webmagic\CustomPages\Entities\CustomPage;
use Webmagic\CustomPages\Repositories\CustomPagesRepo;
use Webmagic\Dashboard\Dashboard;


class CustomPagesDashboardServiceProvider extends CustomPagesServiceProvider
{
    /**
     * Register pages service
     */
    public function register()
    {
        parent::register();

        $this->mergeConfigFrom(
            __DIR__ . '/config/custom_pages_dashboard.php', 'webmagic.dashboard.custom_pages'
        );
    }

    /**
     * Boot pages service
     * @param Router $router
     */
    public function boot(Router $router)
    {
        parent::boot($router);

        $this->loadTranslation();

        //Routs registering
        $this->loadRoutesFrom(__DIR__.'/Http/routes_dashboard.php');

        //Config publishing
        $this->publishes([
            __DIR__ . '/config/custom_pages_dashboard.php' => config_path('webmagic/dashboard/custom_pages.php'),
        ], 'config');

        //Views publishing
        $this->publishes([
            __DIR__.'/resources/views/' => base_path('resources/views/vendor/custom_pages/'),
        ], 'views');

        //Publish translations
        $this->publishes([
            __DIR__.'/resources/lang/' => resource_path('lang/vendor/custom_pages'),
        ]);

        //Add items menu
        $this->includeMenuForDashboard();

        // Load pages in menu if this set in config
        if(config('webmagic.dashboard.custom_pages.add_pages')){
            $this->addPagesMenuItem();
        }
    }


    /**
     * Including menu items for new dashboard
     */
    protected function includeMenuForDashboard()
    {
        if(!$menu_item_config = config('webmagic.dashboard.custom_pages.dashboard_menu_item')){
            return;
        }

        //Add menu into global menu
        $dashboard = app()->make(Dashboard::class);
        $dashboard->getMainMenu()->addMenuItems($menu_item_config);
    }

    /**
     * Including menu items for new dashboard
     */
    protected function addPagesMenuItem()
    {
        //Check if custom_pages table exists
        //todo should be rewrite. Table should be loaded form model or like so
        if(!Schema::hasTable('custom_pages')){
            return;
        }

        /** @var CustomPagesRepo $customPageRepo */
       $customPageRepo = app()->make(CustomPagesRepo::class);
       $pages = $customPageRepo->getAll();

       $pagesMenuItems = [
           'text' => 'custom-pages::common.pages',
           'icon' => 'fa-list'
       ];
       $pages->each(function (CustomPage $page) use (&$pagesMenuItems) {
            $pagesMenuItems['subitems'][] = [
                'text' => $page->title,
                'link' => $page->present()->editPageUrl(),
                'active_rules' => [
                    'urls' => $page->present()->editPageUrl()
                ]
            ];
       });

        //Add menu into global menu
        $dashboard = app()->make(Dashboard::class);
        $dashboard->getMainMenu()->addMenuItems($pagesMenuItems);
    }



    /**
     * Load translation
     */
    public function loadTranslation()
    {
        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'custom-pages');
    }

}