<?php


namespace Webmagic\CustomPages\FieldGroup;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Webmagic\Core\Entity\EntityRepo;
use Webmagic\CustomPages\Field\FieldRepo;

class FieldGroupRepo extends EntityRepo
{
    protected $entity = FieldGroup::class;

    /**
     * Get entity by param page_key
     *
     * @param $page_key
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Pagination\Paginator
     * @internal param int|string $entity_id
     *
     */
    public function getAllByPageKey($page_key)
    {
        $query = $this->query();

        $query->where('page_key', $page_key);

        return $this->realGetMany($query);
    }

    /**
     * Get parents by param page_key
     *
     * @param $page_key
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Pagination\Paginator
     * @internal param int|string $entity_id
     *
     */
    public function getAllParentByPageKey($page_key)
    {
        $query = $this->query();

        $query->where('page_key', $page_key)->whereNull('parent_id')->with('groups');

        return $this->realGetMany($query);
    }

    /**
     * Get parents by param page_key
     *
     * @param $page_key
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Pagination\Paginator
     * @internal param int|string $entity_id
     *
     */
    public function getAllChildByPageKey($page_key)
    {
        $query = $this->query();

        $query->where('page_key', $page_key)->whereNotNull('parent_id');

        return $this->realGetMany($query);
    }

    /**
     * Save field with assigning to FieldGroup entity
     *
     * @param $field_data
     * @param $key
     */
    public function saveWithAssign($field_data, $key)
    {
        $field_repo = app()->make(FieldRepo::class);
        // remove part _child from key this is our parent group key
        $key = $this->getParentKey($key);

        // get parent group with all field_groups
        $parent_field_group = $this->getByKeyWithGroups($key);

        // each group except parent will save field
        foreach ($parent_field_group['groups'] as $field_group){
            $field = $field_repo->create($field_data);

            $field_group->fields()->save($field);
        }
    }


    public function getIdOfGroupsAndField(Model $item, $is_field_group = true)
    {
        $field_groups_to_delete = [];
        $fields_to_delete = [];

        $item_groups = $item['groups'];

        // if its page set collection of field_groups
        if(!$is_field_group){
            $item_groups = $item['fieldGroup'];
        }

        // all groups which related with parent group
        // get their id for delete
        foreach ($item_groups as $group){

            $field_groups_to_delete[] = $group['id'];

            // get fields id which related with each group
            // to delete
            foreach ($group['fields'] as $field){
                $fields_to_delete[] = $field['id'];
            }
        }
        if($is_field_group) {
            // add parent group id to remove
            $field_groups_to_delete[] = $item['id'];
        }


        return [
            'field_groups_to_delete' => $field_groups_to_delete,
            'fields_to_delete' => $fields_to_delete
        ];
    }

    /**
     * Save created field with assign to FieldGroup
     *
     * @param $field
     * @param $key
     */
    public function saveWithAssignForOneGroup($field, $key)
    {
        $field_group = $this->getByKeyWithGroups($key);
        $field_group->fields()->save($field);
    }

    /**
     * Cut part of string and return parent key
     *
     * @param $key
     * @return string
     */
    public function getParentKey($key)
    {
        $found_position = strpos($key, '_child');

        if($found_position === false){
            return $key;
        }

        return substr($key, 0, $found_position);

    }


    /**
     * Get entity by param page_key
     *
     * @param $key
     * @return \Illuminate\Database\Eloquent\Model
     * @internal param $page_key
     * @internal param int|string $entity_id
     */
    public function getByKey($key)
    {

        $query = $this->query();
        $query->where('key', $key);

        return $this->realGetOne($query);
    }


    /**
     * Get entity by param page_key with fields
     *
     * @param $key
     * @return \Illuminate\Database\Eloquent\Model
     * @internal param $page_key
     * @internal param int|string $entity_id
     */
    public function getByKeyWithGroups($key)
    {

        $query = $this->query();
        $query->where('key', $key)->with('groups');

        return $this->realGetOne($query);
    }
    /**
     * Get one entity based on query
     *
     * @param Builder $query
     *
     * @return Model|null
     */
    protected function realGetOne(Builder $query)
    {
        $query = $query->with('fields');

        return $query->first();
    }

    /**
     *  Get all entities based on query
     *
     * @param Builder $query
     *
     * @return Collection|Paginator
     */
    protected function realGetMany(Builder $query)
    {
        return $query->get();
    }

}