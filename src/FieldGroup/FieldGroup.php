<?php

namespace Webmagic\CustomPages\FieldGroup;

use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;

class FieldGroup extends Model
{

    use PresentableTrait;

    /**
     * Presenter class that using for present model
     * @var FieldGroupPresenter
     */
    protected $presenter = FieldGroupPresenter::class;


    /**
     * Hide params
     */
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = ['field_group', 'name', 'key', 'position', 'page_key', 'parent_id'];


    /**
     * Relation with Field
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function fields()
    {
        return $this->morphMany('Webmagic\CustomPages\Field\Field', 'fieldable', 'fieldable_type', 'page_key', 'key');
    }

    /**
     * return related Page
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customPage()
    {
        return $this->belongsTo('Webmagic\CustomPages\Entities\CustomPage', 'key');
    }

    /**
     * return related parent fieldGroup
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentGroup()
    {
        return $this->belongsTo('Webmagic\CustomPages\FieldGroup\FieldGroup', 'id');
    }

    /**
     * Return fieldGroup with related fieldGroups
     */
    public function groups()
    {
        return $this->hasMany('Webmagic\CustomPages\FieldGroup\FieldGroup', 'parent_id', 'id')->with('fields');
    }


}