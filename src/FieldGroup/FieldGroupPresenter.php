<?php


namespace Webmagic\CustomPages\FieldGroup;


use Laracasts\Presenter\Presenter;

class FieldGroupPresenter  extends Presenter
{

    /**
     * Cut part of string and return parent key
     *
     * @return string
     */
    public function getParentKey()
    {
        $key = $this->entity->key;

        $found_position = strpos($key, '_child');

        if($found_position === false){
            return $key;
        }

        return substr($key, 0, $found_position);

    }

    /**
     * Prepare all relations with keyBy
     */
    public function prepareCase()
    {
        $this->entity->fields = $this->entity->fields->keyBy('field_key');
        $this->entity->groups = $this->entity->groups->keyBy('key');

        foreach ($this->entity->groups as $group){
            $group->present()->prepareCase;
        }
    }
}