<?php

namespace Webmagic\CustomPages\Field;

use Illuminate\Database\Eloquent\Model;
use Webmagic\Core\Presenter\PresentableTrait;

class Field extends Model
{
    use PresentableTrait;

    protected $table = 'fields';

    /**
     * Presenter class that using for present model
     * @var
     */
    protected $presenter = FieldPresenter::class;

    /**
     * Hide params
     */
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = ['name', 'slug', 'page_slug', 'position', 'type', 'content', 'page_key', 'field_key', 'fieldable_type', 'fieldable_id'];


    public function fieldable()
    {
        return $this->morphTo();
    }

    /**
     *
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @return EloquentQueryBuilder
     */
    public function newEloquentBuilder($query)
    {
        return new EloquentQueryBuilder($query);
    }

}
