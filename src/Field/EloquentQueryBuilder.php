<?php

namespace Webmagic\CustomPages\Field;


use Illuminate\Database\Eloquent\Builder;

class EloquentQueryBuilder extends Builder
{
    /**
     * Create a collection of models from plain arrays.
     * !!!
     * Redeclare base laravel realisation to have ability to change entity
     * in depend of data which we get from DB
     *
     * @param  array  $items
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function hydrate(array $items)
    {
        $instance = $this->newModelInstance();

        return $instance->newCollection(array_map(function ($item) use ($instance) {

            // get array with config
            $fields_type = config('webmagic.custom_pages.fields_type');

            // get instance class if no class set by default Field
            // in depend of attribute type which we get from DB and our config
            // we will set entity class for this data.
            $instance_class = $fields_type[$item->type]['class'] ? $fields_type[$item->type]['class'] : Field::class;

            //set like a model our class
            $this->setModel(app()->make($instance_class));

            $instance = $this->newModelInstance();

            return $instance->newFromBuilder($item);
        }, $items));
    }
}