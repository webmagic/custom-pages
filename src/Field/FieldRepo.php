<?php


namespace Webmagic\CustomPages\Field;


use Webmagic\Core\Entity\EntityRepo;
use Webmagic\CustomPages\Field\Field;

class FieldRepo extends EntityRepo
{
    protected $entity = Field::class;

    /**
     * Get entity by param page_key
     *
     * @param $page_key
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Pagination\Paginator
     * @internal param int|string $entity_id
     *
     */
    public function getAllByPageKey($page_key)
    {
        $query = $this->query();

        $query->where('page_key', $page_key);

        return $this->realGetMany($query);
    }

    /**
     * Remove certain field in all related FieldGroup
     *
     * @param $group_key
     * @param $field_key
     */
    public function removeAllFieldsInAllGroupsType($group_key, $field_key)
    {
        $query = $this->query();
        // get all fields in related field_groups and certain key
        $query->where( 'page_key', 'like', "$group_key%")->where('field_key', $field_key);

        $fields = $this->realGetMany($query);

        $fields_id_to_remove = [];

        // get all id which should be removed
        foreach ($fields as $field){
            $fields_id_to_remove[] = $field['id'];
        }

        //remove all
        $this->destroy($fields_id_to_remove);
    }



}