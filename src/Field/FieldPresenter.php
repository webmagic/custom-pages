<?php


namespace Webmagic\CustomPages\Field;

use Carbon\Carbon;
use Webmagic\Core\Presenter\Presenter;
use Webmagic\Dashboard\Generators\ComponentGenerator;

class FieldPresenter extends Presenter
{
    /**
     * @var ComponentGenerator
     */
    protected $component_generator;

    function __construct($entity)
    {
        parent::__construct($entity);

        $this->component_generator = app()->make(ComponentGenerator::class);
    }

    /**
     * Getting rendered view
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function getView()
    {
        return $this->prepareView($this->entity->type);
    }

    /**
     * Determ
     *
     * @param $type
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    protected function prepareView($type)
    {
        $view_path = 'custom_pages::dashboard.components.'.$type.'_field';


        if (view()->exists($view_path)) {
            return view($view_path, ['field'=> $this->entity])->render();
        }
    }


    /**
     * Prepare and get content
     */
    public function getContent()
    {
        return $this->prepareContent($this->entity->type);
    }

    protected function prepareContent($type)
    {
        switch ($type) {
            case 'image':
                return $this->prepareImageURL($this->entity->content);
                break;
            case 'gallery':
                return $this->prepareGallery($this->entity->content);
                break;
            case 'file':
                return $this->prepareFileURL($this->entity->content);
                break;
            case 'date_time':
                return $this->prepareDateTime($this->entity->content);
                break;
            default:
                return $this->entity->content;
        }
    }


    /**
     * Prepare url for images
     *
     * @param $file_name
     *
     * @return string
     */
    protected function prepareImageURL($file_name)
    {
        return asset(config('webmagic.custom_pages.img_path') . $file_name);
    }


    /**
     * Prepare URLs for gallery images
     *
     * @return array
     */
    public function prepareGallery($gallery_content)
    {
        $prepared_image = [];

        if($gallery_content) {
            if ($prepared_image = explode('|', $gallery_content)) {
                foreach ($prepared_image as &$image) {
                    if (strlen($image) > 0) {
                        $image = $this->prepareImageURL($image);
                    } else {
                        $image = '';
                    }
                }

                $prepared_image = strlen($prepared_image[0]) > 0 ? $prepared_image : [];
            }
        }

        return $prepared_image;
    }

    /**
     * Prepare url for files
     *
     * @param $file_name
     *
     * @return string
     */
    protected function prepareFileURL($file_name)
    {
        return asset(config('webmagic.custom_pages.file_path') . '/' . $file_name);
    }

    /**
     * Return Carbon object
     *
     * @param $content
     *
     * @return static
     */
    protected function prepareDateTime($content)
    {
        return Carbon::parse($content);
    }

}