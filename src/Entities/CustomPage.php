<?php

namespace Webmagic\CustomPages\Entities;

use Illuminate\Database\Eloquent\Model;
use Webmagic\Core\Presenter\PresentableTrait;
use Webmagic\Core\Presenter\Presenter;

/**
 * Class CustomPage
 * @package Webmagic\CustomPages\Entities
 *
 * @method CustomPagesPresenter present()
 */
class CustomPage extends Model
{

    use PresentableTrait;


    /** @var  Presenter class that using for present model */
    protected $presenter = CustomPagesPresenter::class;


    /**
     * Hide params
     */
    protected $hidden = ['created_at', 'updated_at'];


    /**
     *  The attributes that are mass assignable.
     */
    protected $fillable = ['title', 'slug', 'page_key', 'active', 'position', 'meta_title', 'meta_description', 'meta_keywords', 'key'];


    /**
     * Relation with Field
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function fields()
    {
        return $this->morphMany('Webmagic\CustomPages\Field\Field', 'fieldable', 'fieldable_type', 'page_key', 'key');
    }

    public function fieldGroup()
    {
        return $this->hasMany('Webmagic\CustomPages\FieldGroup\FieldGroup', 'page_key', 'key')->with('groups');;
    }

}