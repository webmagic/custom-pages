<?php

namespace Webmagic\CustomPages\Entities;


use Illuminate\Database\Eloquent\Model;
use Webmagic\Core\Presenter\PresentableTrait;
use Webmagic\Core\Presenter\Presenter;

class Widget extends Model
{

    use PresentableTrait;

    /** @var  Presenter class that using for present model */
    protected $presenter = WidgetPresenter::class;

    /**
     * Hide params
     */
    protected $hidden = ['created_at', 'updated_at'];

    /**
     *  The attributes that are mass assignable.
     */
    protected $fillable = ['name', 'slug', 'content'];

    /**
     * Product constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->fillable = array_merge($this->fillable, config('webmagic.custom_pages.widgets_available_fields'));

        parent::__construct($attributes);
    }
}