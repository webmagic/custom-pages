<?php


namespace Webmagic\CustomPages\Entities;

use Webmagic\Core\Presenter\Presenter;

class CustomPagesPresenter extends Presenter
{
    /**
     * Main image
     *
     * @return string
     */
    public function mainImage()
    {
        return $this->prepareImageURL($this->main_image);
    }


    /**
     * Prepare URLs for gallery images
     *
     * @return array
     */
    public function gallery()
    {
        if (!isset($this->gallery)) {
            if ($images = explode('|', $this->images)) {
                foreach ($images as &$image) {
                    if (strlen($image) > 0) {
                        $image = $this->prepareImageURL($image);
                    } else {
                        $image = '';
                    }
                }

                $this->gallery = strlen($images[0]) > 0 ? $images : [];
            }
        }

        return $this->gallery;
    }


    /**
     * Prepare url for images
     *
     * @param $file_name
     *
     * @return string
     */
    protected function prepareImageURL($file_name)
    {
        return asset(config('webmagic.custom_pages.custom_pages_img_path') . '/' . $file_name);
    }


    /**
     * Prepare all relations with keyBy
     */
    public function prepareCase()
    {

        $this->entity->fields = $this->entity->fields->keyBy('field_key');
        $this->entity->fieldGroup = $this->entity->fieldGroup->keyBy('key');

        foreach ($this->entity->fieldGroup as $group){
            $group->present()->prepareCase;
        }
    }

    /**
     * URL for adit page in dashboard
     *
     * @return string
     */
    public function editPageUrl()
    {
        return route('custom_pages_module::pages::edit', $this->entity->key);
    }

}