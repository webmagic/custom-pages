<?php

namespace Webmagic\CustomPages\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Routing\Controller;
use Webmagic\CustomPages\Entities\CustomPage;
use Webmagic\CustomPages\Field\Field;
use Webmagic\CustomPages\FieldGroup\FieldGroupRepo;
use Webmagic\CustomPages\Repositories\CustomPagesRepo;
use Webmagic\CustomPages\Http\Requests\CustomPagesRequest;
use Webmagic\CustomPages\Field\FieldRepo;
use Webmagic\Dashboard\Generators\ComponentGenerator;

class CustomPagesDashboardController extends Controller
{

    /**
     * Show pages list
     *
     * @param CustomPagesRepo $pagesRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(CustomPagesRepo $pagesRepo)
    {
        $pages = $pagesRepo->getAll();

        $content = view('custom_pages::dashboard.pages', compact('pages'));

        return $this->getDashboardWithContent($content);
    }

    /**
     * Show form for page creating
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $page = '';

        $routes_registration_status = config('webmagic.custom_pages.pages_routes_register');
        $page_url_prefix = config('webmagic.custom_pages.prefix_for_custom_pages');


        $content = view('custom_pages::dashboard.create', compact('page', 'routes_registration_status', 'page_url_prefix'));

        return $this->getDashboardWithContent($content);
    }

    /**
     * Creating page
     *
     * @param CustomPagesRequest $request
     * @param CustomPagesRepo $pagesRepo
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(CustomPagesRequest $request, CustomPagesRepo $pagesRepo)
    {
        if(!$page = $pagesRepo->create($request->all())){
            return response('При создании страницы возникли ошибки', 500);
        }
    }

    /**
     * Show form for creating page
     * @param $page_key
     * @param CustomPagesRepo $pagesRepo

     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     * @internal param $page_id
     */
    public function edit($page_key, CustomPagesRepo $pagesRepo, FieldGroupRepo $field_group_repo)
    {
        if(!$page = $pagesRepo->getByKey($page_key)){
            return response('Cтраница не найдена', 404);
        }

        $fields = $page['fields'];

        // get parent field_groups with their related groups
        $field_groups = $field_group_repo->getAllParentByPageKey($page_key);

        $routes_registration_status = config('webmagic.custom_pages.pages_routes_register');
        $page_url_prefix = config('webmagic.custom_pages.prefix_for_custom_pages');

        $content = view('custom_pages::dashboard.edit',
            compact(
            'page',
            'routes_registration_status',
            'page_url_prefix',
            'fields',
            'field_groups'
            )
        );

        return $this->getDashboardWithContent($content);
    }

    /**
     * @param $page_key
     * @param CustomPagesRepo $pages_repo
     * @param FieldGroupRepo $field_group_repo
     * @return \Illuminate\Contracts\Routing\ResponseFactory|mixed|\Symfony\Component\HttpFoundation\Response
     */
    public function configure($page_key, CustomPagesRepo $pages_repo, FieldGroupRepo $field_group_repo)
    {
        if(!$page = $pages_repo->getByKey($page_key)){
            return response('Cтраница не найдена', 404);
        }

        // get parent field_groups with their related groups
        $fields_group = $field_group_repo->getAllParentByPageKey($page_key);

        // fields by relation
        $fields = $page['fields'];
        // type of entity
        $item_type = 'page';

        $content = view('custom_pages::dashboard.configure',
            compact(
                'page',
                'fields',
                'fields_group',
                'item_type'
            )
        );

        return $this->getDashboardWithContent($content);
    }


    /**
     * Update page by ID
     * @param $page_id
     * @param CustomPagesRequest $request
     * @param CustomPagesRepo $pagesRepo
     * @param FieldRepo $fields_repo
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update($page_id, CustomPagesRequest $request, CustomPagesRepo $pagesRepo, FieldRepo $fields_repo)
    {
        $data_request = $request->all();

        $fields_data =  $data_request['field'];

        // each field update param content by id
        if(isset($fields_data) && count($fields_data)){
            foreach ($fields_data as $field){
                $field_id = key($field);
                $field_value = reset($field);

                // check if we have file with this key
                // if yes and value is null skip this
                if($request->has('file_'.$field_id.'_update') && !$request['file_'.$field_id.'_update']){
                    continue;
                }
                // check if we have file with this key
                // prepare this file
                if($request->has('file_'.$field_id.'_update')) {


                    $path = config('webmagic.custom_pages.img_path');

                    $req_prepared_images = $this->imagesPrepare($request, 'file_' . $field_id, $path);
                    $field_value = $req_prepared_images['file_'.$field_id];
                }

                // update field data
                $fields_repo->update($field_id, array('content' => $field_value));
            }
        }

        if(!$pagesRepo->update($page_id, $data_request)){
            return response('При обновлении страницы возникли ошибки', 500);
        }
    }


    /**
     * Destroy page
     * @param $page_key
     * @param CustomPagesRepo $page_repo
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @internal param $page_id
     * @internal param CustomPagesRepo $pagesRepo
     */
    public function destroy($page_key, CustomPagesRepo $page_repo, FieldGroupRepo $field_group_repo, FieldRepo $field_repo)
    {
        $page = $page_repo->getByKey($page_key);

        if(empty($page)){
            return response('Страница не найдена', 404);
        }

        // get id of fields  related to field_groups
        $field_related_to_group = $field_group_repo->getIdOfGroupsAndField($page, false);

        // get id of related to page fields
        $field_related_to_page = $page_repo->getIdOfRelatedFields($page);

        // join arrays
        $fields_to_delete = array_merge($field_related_to_group['fields_to_delete'] , $field_related_to_page);

        // remove all fields which related with group which related with page
        $field_repo->destroy($fields_to_delete);

        $page_repo->destroy($page['id']);
    }


    /**
     * Move images and prepare URLs for DB saving
     *
     * @param Request $request
     * @param bool $field_name
     * @param $path
     * @return Request
     */
    protected function imagesPrepare(Request $request, $field_name, $path)
    {
        // check if
        // request has mark for updated needed field
        // and value of first item was set
        $field_update_key = $field_name.'_update';

        if($request->has($field_update_key) && $request[$field_update_key] === 'true' && $request->has($field_name.'0')){

            $file_key = 0;
            $file_name = $field_name.$file_key;
            $names_compilation = '';

            //save all items of field name
            while($request->file($file_name)){

                //add uniq ID if functionality on in config
                $real_file_name = $request[$file_name]->getClientOriginalName();
                $real_file_name = config('webmagic.dashboard.custom_pages.hash_use') ? uniqid() . $real_file_name : $real_file_name;
                $names_compilation .= $request[$file_name]->move(public_path($path), $real_file_name )->getFilename() . "|";

                $file_key++;
                $file_name = $field_name.$file_key;
            }

            $request[$field_name] = rtrim($names_compilation, '|');

        } else {
            //we should do nothing and remove key from request
            unset($request[$field_name]);
        }

        return $request;
    }

    /**
     * Prepare Dashboard and set content inside
     *
     * @param $content
     * @return mixed
     */
    public function getDashboardWithContent($content)
    {
        $dashboard = app()->make(\Webmagic\Dashboard\Dashboard::class);
        $dashboard->content($content);
        return $dashboard->render();
    }

}