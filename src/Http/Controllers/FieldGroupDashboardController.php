<?php


namespace Webmagic\CustomPages\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Webmagic\CustomPages\Field\FieldRepo;
use Webmagic\CustomPages\FieldGroup\FieldGroupRepo;

class FieldGroupDashboardController extends Controller
{

    /**
     * Create view
     *
     * @param $page_key
     * @return mixed
     */
    public function create($page_key)
    {
        $content = view(
            'custom_pages::dashboard.field_group.create',
            compact('page_key')
        );

        return $this->getDashboardWithContent($content);
    }

    /**
     * Store field_group
     *
     * @param Request $request
     * @param FieldGroupRepo $field_group_repo
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request, FieldGroupRepo $field_group_repo)
    {
        $req = $request->all();

        // create new field it will be parent group
        $field_group_parent = $field_group_repo->create($req);

        // create new key with adding new key and set parent id
        $req['parent_id'] = $field_group_parent['id'];
        $req['key'] = $field_group_parent['key'] . '_child';
        $field_group = $field_group_repo->create($req);

        if(!$field_group || !$field_group_parent){
            return response('При создании страницы возникли ошибки', 500);
        }

    }

    /**
     * On edit view
     *
     * @param $field_group_key
     * @param FieldGroupRepo $field_group_repo
     * @return \Illuminate\Contracts\Routing\ResponseFactory|mixed|\Symfony\Component\HttpFoundation\Response
     */
    public function edit($field_group_key, FieldGroupRepo $field_group_repo)
    {

        if(!$field_group = $field_group_repo->getByKey($field_group_key)){
            return response('Группа полей не найдена', 404);
        }

        $page_key = $field_group['page_key'];

        // fields by relation
        $fields = $field_group['fields'];
        // type of entity
        $item_type = 'group';

        $content = view(
            'custom_pages::dashboard.field_group.edit',
            compact('field_group', 'page_key', 'fields', 'item_type')
        );

        return $this->getDashboardWithContent($content);

    }

    /**
     * Updating data
     *
     * @param $field_id
     * @param FieldGroupRepo $field_group_repo
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update($field_id, FieldGroupRepo $field_group_repo, Request $request)
    {
        if(!$field_group_repo->update($field_id, $request->all())){
            return response('При обновлении поля возникли ошибки', 500);
        }
    }


    /**
     * Destroy
     *
     * @param $field_group_key
     * @param FieldGroupRepo $field_group_repo
     * @param FieldRepo $field_repo
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy($field_group_key, FieldGroupRepo $field_group_repo, FieldRepo $field_repo)
    {
        $field_group = $field_group_repo->getByKey($field_group_key);

        if(empty($field_group)){
            return response('Группа полей не найдена', 404);
        }

        // array of elements which need to destroy
        $destroy_fields = [];

        // start to creating all fields for new group
        foreach ($field_group['fields'] as $field){
            $destroy_fields[] =  $field['id'];
        }

        //remove all fields which related with this group
        $field_repo->destroy($destroy_fields);

        $field_group_repo->destroy($field_group['id']);
    }

    /**
     * Destroy parent group with cascade of destroy child groups and their fields
     *
     * @param $field_group_key
     * @param FieldGroupRepo $field_group_repo
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroyParentGroup($field_group_key, FieldGroupRepo $field_group_repo, FieldRepo $field_repo)
    {
        $field_group = $field_group_repo->getByKeyWithGroups($field_group_key);

        if(empty($field_group)){
            return response('Группа полей не найдена', 404);
        }

        $to_delete = $field_group_repo->getIdOfGroupsAndField($field_group);

        // call repos for remove field_groups and fields
        $field_group_repo->destroy($to_delete['field_groups_to_delete']);
        $field_repo->destroy($to_delete['fields_to_delete']);
    }


    /**
     * Copy entity with fields
     *
     * @param $key
     * @param FieldGroupRepo $field_group_repo
     * @param FieldRepo $field_repo
     * @return \Illuminate\Http\RedirectResponse
     */
    public function copy($key, FieldGroupRepo $field_group_repo, FieldRepo $field_repo)
    {
        $field_group_parent = $field_group_repo->getByKeyWithGroups($key);

        $last_added_element = $field_group_parent['groups']->last();

        // this is key parent + _child will be first child element
        $key = $key . '_child';

        // get first child group
        $field_group = $field_group_repo->getByKey($key);

        // get all data of group
        $field_group_data = $field_group->toArray();

        // get fields of group
        $field_group_fields = $field_group_data['fields'];

        // remove fields array from data
        unset($field_group_data['fields']);

        // create new key for new group will be like 'group_name_child_2'
        $field_group_data['key'] = $field_group_data['key'] . '_' . $last_added_element['id'];

        //create new field on
        $new_field_group = $field_group_repo->create($field_group_data);


        // start to creating all fields for new group
        foreach ($field_group_fields as $field){

            //clear content
            $field['content'] = '';

            $new_field = $field_repo->create($field);

            $field_group_repo->saveWithAssignForOneGroup($new_field, $new_field_group['key']);
        }

        return  back();

    }


    /**
     * Prepare Dashboard and set content inside
     *
     * @param $content
     * @return mixed
     */
    public function getDashboardWithContent($content)
    {
        $dashboard = app()->make(\Webmagic\Dashboard\Dashboard::class);
        $dashboard->content($content);
        return $dashboard->render();
    }
}