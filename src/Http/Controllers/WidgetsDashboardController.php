<?php

namespace Webmagic\CustomPages\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Webmagic\CustomPages\Repositories\WidgetRepo;

class WidgetsDashboardController extends Controller
{

    /**
     * Creating widget
     *
     * @param Request $request
     * @param WidgetRepo $widgetRepo
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request, WidgetRepo $widgetRepo)
    {
        if(!$widgetRepo->create($request->all())){
            return response('При создании страницы возникли ошибки', 500);
        }
    }

    /**
     * Show widgets list
     *
     * @param WidgetRepo $widgetRepo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(WidgetRepo $widgetRepo)
    {
        $widgets = $widgetRepo->getAll();
        return view('custom_pages::dashboard.widgets.widgets', compact('widgets'));
    }

    /**
     * Show form for widget creating
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $widget = '';
        return view('custom_pages::dashboard.widgets.create', compact( 'widget'));
    }

    /**
     * Show form for creating widget
     * @param $widget_id
     * @param WidgetRepo $widgetRepo
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function edit($widget_id, WidgetRepo $widgetRepo)
    {
        if(!$widget = $widgetRepo->getByID($widget_id)){
            return response('Товар не найден', 404);
        }
        
        return view('custom_pages::dashboard.widgets.edit', compact( 'widget'));
    }


    /**
     * Update widget by ID
     * @param $widget_id
     * @param Request $request
     * @param WidgetRepo $widgetRepo
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update($widget_id, Request $request, WidgetRepo $widgetRepo)
    {
        if(!$widgetRepo->update($widget_id, $request->all())){
            return response('При обновлении страницы возникли ошибки', 500);
        }
    }


    /**
     * Destroy widget
     * @param $widget_id
     * @param WidgetRepo $widgetRepo
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy($widget_id, WidgetRepo $widgetRepo)
    {
        if(!$widget = $widgetRepo->destroy($widget_id)){
            return response('Товар не найден', 404);
        }
    }



}