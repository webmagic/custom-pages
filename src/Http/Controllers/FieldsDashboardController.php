<?php

namespace Webmagic\CustomPages\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Webmagic\CustomPages\FieldGroup\FieldGroup;
use Webmagic\CustomPages\FieldGroup\FieldGroupRepo;
use Webmagic\CustomPages\Http\Requests\FieldRequest;
use Webmagic\CustomPages\Repositories\CustomPagesRepo;
use Webmagic\CustomPages\Field\FieldRepo;


class FieldsDashboardController extends Controller
{
    /**
     * Show form for page creating
     *
     * @param $page_key
     * @param CustomPagesRepo $page_repo
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @internal param $page_id
     */
    public function create($item_type, $item_key, CustomPagesRepo $page_repo)
    {
        if($item_type === 'page' && !$page = $page_repo->getByKey($item_key)){
            return response('Cтраница не найденa', 404);
        }

        $field = '';

        $fields_type = config('webmagic.custom_pages.fields_type');

        $content = view(
            'custom_pages::dashboard.field.create',
            compact('field','item_type', 'item_key', 'fields_type')
        );

        return $this->getDashboardWithContent($content);
    }

    /**
     * Creating field
     *
     * @param Request|FieldRequest $request
     * @param FieldRepo $fields_repo
     * @param CustomPagesRepo $custom_pages_repo
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    //todo validation
    public function store(Request $request, FieldRepo $fields_repo, CustomPagesRepo $custom_pages_repo, FieldGroupRepo $field_group_repo)
    {
        $req = $request->all();

        //todo replace this with validation
        if(empty($req['item_type'])){
            return response('The field item_type is', 500);
        }

        // check with which one Entity should be related and assign

        if($req['item_type'] === 'page'){
            $field = $fields_repo->create($req);
            $custom_pages_repo->saveWithAssign($field, $req['item_key']);
        }

        if($req['item_type'] === 'group'){
            $field_group_repo->saveWithAssign($req, $req['item_key']);
        }

    }


    /**
     * Editing field
     *
     * @param $field_id
     * @param FieldRepo $field_repo
     * @return \Illuminate\Contracts\Routing\ResponseFactory|mixed|\Symfony\Component\HttpFoundation\Response
     */
    public function edit($field_id, FieldRepo $field_repo)
    {

        if(!$field = $field_repo->getByID($field_id)){
            return response('Поле не найдено', 404);
        }


        $fields_type = config('webmagic.custom_pages.fields_type');

        $item_key = $field['page_key'];

        $item_type = 'page';

        // check relation and change if need
        if($field['fieldable_type'] == FieldGroup::class) {
            $item_type = 'group';
        }

        $content = view(
            'custom_pages::dashboard.field.edit',
            compact('field', 'page_key', 'fields_type', 'item_key', 'item_type')
        );

        return $this->getDashboardWithContent($content);

    }

    /**
     * Update field by id
     *
     * @param $field_id
     * @param FieldRepo $fields_repo
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @internal param $page_id
     */
    public function update($field_id, FieldRepo $fields_repo, Request $request)
    {
        if(!$fields_repo->update($field_id, $request->all())){
            return response('При обновлении поля возникли ошибки', 500);
        }
    }


    /**
     * @param $field_id
     * @param FieldRepo $fields_repo
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy($field_id, FieldRepo $fields_repo)
    {
        $field = $fields_repo->getById($field_id);

        // if it related with FieldGroup
        // call to remove all fields which related with related fieldGroups
        if($field['fieldable_type'] == FieldGroup::class){

            $fields_repo->removeAllFieldsInAllGroupsType($field['page_key'], $field['field_key']);

            return response('',200);
        }

        if(!$fields_repo->destroy($field_id)){
            return response('Поле не найдена', 404);
        }
    }


    /**
     * Prepare Dashboard and set content inside
     *
     * @param $content
     * @return mixed
     */
    public function getDashboardWithContent($content)
    {
        $dashboard = app()->make(\Webmagic\Dashboard\Dashboard::class);
        $dashboard->content($content);
        return $dashboard->render();
    }
}