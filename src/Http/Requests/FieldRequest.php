<?php

namespace Webmagic\CustomPages\Http\Requests;


use Illuminate\Foundation\Http\FormRequest as Request;


class FieldRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        // extends Validator only for this request
        \Validator::extend( 'composite_unique', function ( $attribute, $value, $parameters, $validator ) {

            // remove first parameter and assume it is the table name
            $table = array_shift( $parameters );

            // start building the conditions
            $fields = [ $attribute => $value ]; // current field, company_code in your case

            // iterates over the other parameters and build the conditions for all the required fields
            while ( $field = array_shift( $parameters ) ) {
                $fields[ $field ] = $this->get( $field );
            }

            // query the table with all the conditions
            $result = \DB::table( $table )->select( \DB::raw( 1 ) )->where( $fields )->first();

            return empty($result); // edited here
        }, 'your custom composite unique key validation message' );

        return [
                'name' => 'required',
                'type' => 'required',
                'page_key' => 'required|composite_unique:fields,field_key',
                'field_key' => 'required|min:1'
        ];
    }

}