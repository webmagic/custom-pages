<?php

Route::group([
    'namespace' => '\Webmagic\CustomPages\Http\Controllers',
    'middleware' => config('webmagic.dashboard.custom_pages.middleware'),
    'prefix' => config('webmagic.dashboard.custom_pages.prefix_for_custom_pages'),
    'as' => 'custom_pages_module::'
],
    function() {
        /**
         * Custom pages routes
         */
        Route::group([
            'as' => 'pages::',
        ], function(){
            Route::resource('page', 'CustomPagesDashboardController', [
                    'except' => ['show'],
                    'names' => [
                        'index' => 'index',
                        'create' => 'create',
                        'store' => 'store',
                        'edit' => 'edit',
                        'update' => 'update',
                        'destroy' => 'destroy',
                    ]]
            );
            Route::get('page/configure/{page_id}',[
                'as' => 'configure',
                'uses'=>'CustomPagesDashboardController@configure'
            ]);
        });

        /**
         * Fields routes
         */
        Route::group([
            'as' => 'fields::',
        ], function(){
            Route::resource('field', 'FieldsDashboardController', [
                    'except' => ['show','create'],
                    'names' => [
                        'index' => 'index',
                        'store' => 'store',
                        'edit' => 'edit',
                        'update' => 'update',
                        'destroy' => 'destroy'
                    ]]
            );

            Route::get('field/create/{item_type}-{item_key}',[
                'as' => 'create',
                'uses'=>'FieldsDashboardController@create'
            ]);
        });

        /**
         * Custom pages routes
         */
        Route::group([
            'as' => 'fields_group::',
        ], function(){
            Route::resource('field-group', 'FieldGroupDashboardController', [
                    'except' => ['show','create'],
                    'names' => [
                        'index' => 'index',
                        'store' => 'store',
                        'edit' => 'edit',
                        'update' => 'update',
                        'destroy' => 'destroy'
                    ]]
            );

            Route::get('field-group/create/{page_key}',[
                'as' => 'create',
                'uses'=>'FieldGroupDashboardController@create'
            ]);


            Route::get('field-group/copy/{page_key}',[
                'as' => 'copy',
                'uses'=>'FieldGroupDashboardController@copy'
            ]);

            Route::delete('field-group/destroy-parent/{field_group_key}',[
                'as' => 'destroy-parent',
                'uses'=>'FieldGroupDashboardController@destroyParentGroup'
            ]);
        });

        /**
         * Widgets routes
         */
        Route::group([
            'as' => 'widgets::',
        ], function(){
            Route::resource('widget', 'WidgetsDashboardController', [
                    'except' => ['show'],
                    'names' => [
                        'index' => 'index',
                        'create' => 'create',
                        'store' => 'store',
                        'edit' => 'edit',
                        'update' => 'update',
                        'destroy' => 'destroy'
                    ]]
            );
        });
    }
);



