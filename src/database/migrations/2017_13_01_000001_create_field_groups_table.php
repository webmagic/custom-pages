<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('key')->nullable();
            $table->integer('parent_id')->nullable();
            $table->string('page_key')->nullable();
            $table->integer('position')->nullable();
            $table->timestamps();

            $table->foreign('page_key')
                ->references('key')
                ->on('custom_pages')
                ->onDelete('cascade');

            $table->unique(['key', 'page_key', 'parent_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('field_groups');
    }
}
