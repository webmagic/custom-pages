<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('page_key')->nullable();
            $table->integer('position')->nullable();
            $table->longText('content')->nullable();
            $table->string('field_key');
            $table->string('type')->default('text');
            $table->timestamps();

            $table->string('fieldable_type')->nullable();

//            $table->unique(['field_key', 'page_key']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fields');
    }
}
