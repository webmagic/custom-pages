<?php

use Illuminate\Database\Seeder;

class WidgetsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\Webmagic\CustomPages\Entities\Widget::class, 3)->create();
    }
}