<?php

use Illuminate\Database\Seeder;

class CustomPagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\Webmagic\CustomPages\Entities\CustomPage::class, 3)->create();
    }
}