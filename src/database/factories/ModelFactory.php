<?php
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/


/**
 * Custom Pages
 */
$factory->define(\Webmagic\CustomPages\Entities\CustomPage::class, function (Faker\Generator $faker) {
    return [
        'title' => $name = $faker->word,
        'slug' => str_slug($name, '-'),
    ];
});


/**
 * Widgets
 */
$factory->define(\Webmagic\CustomPages\Entities\Widget::class, function (Faker\Generator $faker) {
    return [
        'name' => $name = $faker->word,
        'slug' => str_slug($name, '-'),
        'content' => $faker->text(256),
    ];
});