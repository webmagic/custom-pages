<?php

namespace Tests\Unit\Pages;

use Illuminate\Foundation\Auth\User;
use Tests\TestCase;
use Webmagic\CustomPages\Entities\Widget;


class WidgetPagesTest extends TestCase
{

    public function testIndexPage()
    {
        $user = new User();
        $response = $this->actingAs($user)->get(route('widgets::index'));

        $response->assertStatus(200);
    }

    public function testCreatePage()
    {
        $user = new User();
        $response = $this->actingAs($user)->get(route('widgets::create'));

        $response->assertStatus(200);
    }

    public function testEditPage()
    {
        $user = new User();
        $widget = factory(Widget::class)->create();

        $response = $this->actingAs($user)->get(route('widgets::edit', $widget->id));
        $response->assertStatus(200);
    }
}
