<?php

namespace Tests\Unit\Pages;

use Illuminate\Foundation\Auth\User;
use Tests\TestCase;
use Webmagic\CustomPages\Entities\CustomPage;


class CustomPagesTest extends TestCase
{

    public function testIndexPage()
    {
        $user = new User();
        $response = $this->actingAs($user)->get(route('custom_pages::index'));
        $response->assertStatus(200);
    }

    public function testCreatePage()
    {
        $user = new User();
        $response = $this->actingAs($user)->get(route('custom_pages::create'));
        $response->assertStatus(200);
    }

    public function testPageEdit()
    {
        $user = new User();
        $page = factory(CustomPage::class)->create();

        $response = $this->actingAs($user)->get(route('custom_pages::edit', $page->id));
        $response->assertStatus(200);
    }
}





