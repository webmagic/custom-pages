<?php

namespace Tests\Unit\CustomPages;

use Tests\TestCase;
use Webmagic\CustomPages\Entities\CustomPage;


class CustomPagesRepoTest extends TestCase
{
    /**
     * Custom pages
     * @var
     */
    protected $repo;

    public function setUp()
    {
        parent::setUp();

        $this->repo = new CustomPagesRepo();
        $this->repo->setEntity(CustomPage::class);
    }


    public function testGetBySlug()
    {
        $test_slug = 'test';

        $first = factory(CustomPage::class)->create([
            'slug' => $test_slug
        ]);
        factory(CustomPage::class)->create();

        $page = $this->repo->getBySlug($test_slug);
        $this->assertEquals($first->slug, $page->slug);
    }

}





