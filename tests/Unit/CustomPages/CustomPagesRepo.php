<?php

namespace Tests\Unit\CustomPages;

use Webmagic\CustomPages\Repositories\CustomPagesRepo as BaseRepo;

class CustomPagesRepo extends BaseRepo
{
    public function setEntity($entity_name)
    {
        $this->entity = $entity_name;
    }
}