<?php

namespace Tests\Unit\Widget;

use Webmagic\CustomPages\Repositories\WidgetRepo as BaseRepo;

class WidgetRepo extends BaseRepo
{
    public function setEntity($entity_name)
    {
        $this->entity = $entity_name;
    }
}