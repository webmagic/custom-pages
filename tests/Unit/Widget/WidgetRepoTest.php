<?php

namespace Tests\Unit\Widget;

use Tests\TestCase;
use Webmagic\CustomPages\Entities\Widget;


class WidgetRepoTest extends TestCase
{
    /**
     * Widget
     * @var
     */
    protected $repo;

    public function setUp()
    {
        parent::setUp();

        $this->repo = new WidgetRepo();
        $this->repo->setEntity(Widget::class);
    }


    public function testGetBySlug()
    {
        $test_slug = 'test';

        $first = factory(Widget::class)->create([
            'slug' => $test_slug
        ]);
        factory(Widget::class)->create();

        $page = $this->repo->getBySlug($test_slug);
        $this->assertEquals($first->slug, $page->slug);
    }

}





