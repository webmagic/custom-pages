<?php

namespace Tests;


use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Contracts\Console\Kernel;
use Webmagic\CustomPages\CustomPagesDashboardServiceProvider;
use Webmagic\Dashboard\DashboardServiceProvider;

class TestCase extends BaseTestCase
{
    /**
     * Boots the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../vendor/laravel/laravel/bootstrap/app.php';
        $app->make(Kernel::class)->bootstrap();

        $app->register(DashboardServiceProvider::class);
        $app->register(CustomPagesDashboardServiceProvider::class);

        return $app;
    }

    /**
     * Setup DB before each test.
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->artisan('migrate', ['--path' => '../../../src/database/migrations']);
    }
}